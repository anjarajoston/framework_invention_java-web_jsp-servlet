/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Front_controller;

import Annotation.Url_ClassMethod_HashMap;
import ModelView.ModelView;
import Traitement.TraitementUtilitaire;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author User_HP
 */
@WebServlet(name = "FrontServlet", urlPatterns = {"*.do"})
public class FrontServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        /*---------------------------------- Chemain relatif for get Url_ClassMethod_HashMap ----------------------------------*/
        String chemain = this.getServletContext().getRealPath("")+"/WEB-INF/classes/";
        /*--------------------------------------------------------------------*/
        try {
            /* Controller FrontServlet method processRequest */
            
        /*------------------------------- Save in context relation Url-Class/Method -------------------------------------*/
            ServletContext context = this.getServletContext();
            
            /*--------------------------------- Fill Url_ClassMethod_HashMap attributes from context -------------------------------*/
                if( context.getAttribute("UrlClassMethod") == null ){
                    context.setAttribute("UrlClassMethod",TraitementUtilitaire.getUrl_ClassMethod_HashMap(chemain));
                }
            /*----------------------------------------------------------------*/
        /*--------------------------------------------------------------------*/
        
        /*------------------------------- Get Part url utile -------------------------------------*/
            String url = TraitementUtilitaire.retrieveUrlFromRawurl(request.getRequestURI());
        /*--------------------------------------------------------------------*/
        
        try {
            /*-------------------------------- Check Url -----------------------------------*/
                Url_ClassMethod_HashMap classMethod_HashMap = TraitementUtilitaire.checkUrl(url, (ArrayList<Url_ClassMethod_HashMap>)context.getAttribute("UrlClassMethod"));
            /*--------------------------------------------------------------------*/
            /*--------------------------------- Instanciation objet -----------------------------------*/
                String nomClass = classMethod_HashMap.getClas();
                Class clas = Class.forName(nomClass);
                Object obj = clas.getConstructor().newInstance();
            /*--------------------------------------------------------------------*/
            
            /*---------------------------------- Test request.getParameter() et instanciation automatique si oui ----------------------------------*/
                
                /*---------------------------- Get all attribut obj --------------------------------*/
                String [] attribut = TraitementUtilitaire.getAttributObject(obj);
                /*------------------------------------------------------------*/
                
                for (String attribut1 : attribut) {
                    if (request.getParameter(attribut1) != null) {
                        /*--------------------- If request.getParameter() exist, instanciation and give value for attribut --------------------------*/
                        TraitementUtilitaire.instanciation(obj, attribut1, request.getParameter(attribut1));
                        /*---------------------------------------------------*/
                    }
                }
                
            /*--------------------------------------------------------------------*/
            
            /*---------------------------------- Invoke method,setAttribut and dispache ----------------------------------*/
                
                String fonction = classMethod_HashMap.getMethod();
                ModelView mv = (ModelView)obj.getClass().getDeclaredMethod(fonction).invoke(obj);
                request.setAttribute(mv.getData().getNomRecup(), mv.getData().getValeurRecup());
                RequestDispatcher dispat = request.getRequestDispatcher(mv.getUrl());
                dispat.forward(request,response);
            
            /*--------------------------------------------------------------------*/
        } catch (Exception e) {
            out.print(e);
        }
            
        } catch ( Exception e ){
            out.print(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
