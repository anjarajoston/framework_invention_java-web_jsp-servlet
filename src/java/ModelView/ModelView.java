/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelView;

import Annotation.Url_ClassMethod_HashMap;

/**
 *
 * @author User_HP
 */
public class ModelView {
    String url;
    Url_ClassMethod_HashMap data;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Url_ClassMethod_HashMap getData() {
        return data;
    }

    public void setData(Url_ClassMethod_HashMap data) {
        this.data = data;
    }
}
