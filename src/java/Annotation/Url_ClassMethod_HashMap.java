/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Annotation;

import java.util.HashMap;

/**
 *
 * @author User_HP
 */
public class Url_ClassMethod_HashMap extends HashMap{
    String url;
    String clas;
    String method;
    String nomRecup;
    Object valeurRecup;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getClas() {
        return clas;
    }

    public void setClas(String clas) {
        this.clas = clas;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getNomRecup() {
        return nomRecup;
    }

    public void setNomRecup(String nomRecup) {
        this.nomRecup = nomRecup;
    }

    public Object getValeurRecup() {
        return valeurRecup;
    }

    public void setValeurRecup(Object valeurRecup) {
        this.valeurRecup = valeurRecup;
    }
    
    
}
