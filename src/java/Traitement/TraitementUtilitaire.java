/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Traitement;

import Annotation.MyAnnotation;
import Annotation.Url_ClassMethod_HashMap;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author User_HP
 */

/*-------------------------------- Class qui fait les Traitement_Utilitaire du framWork ------------------------------------------*/
public class TraitementUtilitaire {
    
/*------------------------------- Method getPartUrl utile pour les traitements -----------------------------------------*/
    public static String retrieveUrlFromRawurl(String url){
        return url.split("/")[2].split("\\.")[0];
    }
/*----------------------------------------------------------------------------*/
    
/*--------------------------------- Get Url_ClassMethod_HashMap attributes for context -------------------------------------------*/
    
    /*----- Get listes Packages with url -----*/
    public static String [] getPackage(String url){
        File file = new File(url);
        File [] listes = file.listFiles();
        
        int nombre = 0;
        
        for( int i = 0 ; i < listes.length ; i++ ){
            if(listes[i].isDirectory()){
                nombre++;
            }
        }
        
        String [] listesPackages = new String[nombre];
        int n = 0;
        
        for( int i = 0 ; i < listes.length ; i++ ){
            if(listes[i].isDirectory()){
                listesPackages[n] = listes[i].getName();
                n++;
            }
        }        
        return listesPackages;
    }
    /*-----------------------------------------*/
    
    /*----- Get listes Class with url/Package -----*/
    public static String [] getClass(String url){
        String [] listesPackages = getPackage(url);
        
        int nombre = 0;
        
        for( int i = 0 ; i < listesPackages.length ; i++ ){
            File file = new File(url+"/"+listesPackages[i]+"/");
            File [] clas = file.listFiles();
            for( int j = 0 ; j < clas.length ; j++ ){
                if( !clas[j].isDirectory() ){
                    nombre++;
                }
            }
        }
        
        String [] listesClass = new String[nombre];
        int n = 0;
        
        for( int i = 0 ; i < listesPackages.length ; i++ ){
            File file = new File(url+"/"+listesPackages[i]+"/");
            File [] clas = file.listFiles();
            for( int j = 0 ; j < clas.length ; j++ ){
                if( !clas[j].isDirectory() ){
                    listesClass[n] = (clas[j].getName()).split("\\.")[0];
                    n++;
                }
            }
        }
        return listesClass;
    }
    /*-----------------------------------------*/
    /*----- Get Url_ClassMethod_HashMap -----*/
    public static ArrayList<Url_ClassMethod_HashMap> getAnnotationForClass(String packaje,String nomClass)throws Exception{
        try {
            
            String kilasy = packaje+"."+nomClass;
            ArrayList<Url_ClassMethod_HashMap> listesHashmap = new ArrayList<>();
            
            Class clas = Class.forName(kilasy);
            Object obj = clas.getConstructor().newInstance();
            
            for (Method method : obj.getClass().getMethods()) {
                Url_ClassMethod_HashMap hashmap = new Url_ClassMethod_HashMap();
                if (method.getAnnotation(MyAnnotation.class) != null) {
                    hashmap.setUrl(method.getAnnotation(MyAnnotation.class).url());
                    hashmap.setClas(kilasy);
                    hashmap.setMethod(method.getName());
                    listesHashmap.add(hashmap);
                }
            }
            return listesHashmap;
        } catch (Exception ex) {
             throw ex;
        }
    }
    
    public static ArrayList<Url_ClassMethod_HashMap>  getUrl_ClassMethod_HashMap(String url) throws Exception{
        try {
            
            ArrayList<Url_ClassMethod_HashMap> allClass = new ArrayList<>();
            
            String [] pakage = getPackage(url);
            
            String [] clas = getClass(url);
            
            for( int i = 0 ; i < pakage.length ; i++ ){
                for( int j = 0 ; j < clas.length ; j++ ){
                    try {
                        ArrayList<Url_ClassMethod_HashMap> listesHashmap = getAnnotationForClass(pakage[i], clas[j]);
                        for( int k = 0 ; k < listesHashmap.size() ; k++ ){
                            allClass.add(listesHashmap.get(k));
                        }
                    } catch (Exception e) {
                        continue;
                    }
                }
            }
            return allClass;
            
        } catch (Exception e) {
            throw e;
        }
    }
    /*-----------------------------------------*/
    /*--------------------- Check Url --------------------*/
    public static Url_ClassMethod_HashMap checkUrl(String url,ArrayList<Url_ClassMethod_HashMap> url_ClassMethod) throws Exception {
        Url_ClassMethod_HashMap classMethod_HashMap = new Url_ClassMethod_HashMap();
        int count = 0;
        
        try{
            for( int i = 0 ; i < url_ClassMethod.size() ; i++ ){
                if( url_ClassMethod.get(i).getUrl().equals(url)){
                    //continue;
                    classMethod_HashMap = url_ClassMethod.get(i);
                } else {
                    count++;
                }
            }
            if( count == url_ClassMethod.size() ){
                throw new Exception("URL not exist ! Please create URL : "+url);
            }
        } catch(Exception ex){
            throw ex;
        } 
        return classMethod_HashMap;
    }
    /*-----------------------------------------*/
    
    /*-------------------- Get all attribut class ---------------------*/
    public static String [] getAttributObject(Object obj){
        Field [] attribut = obj.getClass().getDeclaredFields();
        String [] attr = new String[attribut.length];
        for( int  i = 0 ; i < attribut.length ; i++ ){
            attr[i] = attribut[i].getName();
        }
        return attr;
    }
    /*-----------------------------------------*/
    
    /*-------------------- Transformation en majuscule 1er lettre attribut pour Getters and Setters ---------------------*/
    public static String enMajuscule(String attr){
        char [] spliter = attr.toCharArray();
        char unMaj = Character.toUpperCase(spliter[0]);
        spliter[0] = unMaj;
        return String.copyValueOf(spliter);
    }
    /*-----------------------------------------*/
    
    /*-------------------- Instaciation automatique and give value ---------------------*/
    public static void instanciation(Object obj,String attribut,String valeur) throws Exception {
        try {
            Class clas = obj.getClass().getDeclaredMethod("get"+enMajuscule(attribut)).getReturnType();
            
            if( clas.equals(new Integer("0").getClass()) || clas.equals(int.class) ){
                obj.getClass().getDeclaredMethod("set"+enMajuscule(attribut),clas).invoke(obj, Integer.parseInt(valeur));
            } else if( clas.equals(new Double("0").getClass()) || clas.equals(double.class) ){
                obj.getClass().getDeclaredMethod("set"+enMajuscule(attribut),clas).invoke(obj, Double.parseDouble(valeur));
            } else if( clas.equals(new Float("0").getClass()) || clas.equals(float.class) ){
                obj.getClass().getDeclaredMethod("set"+enMajuscule(attribut),clas).invoke(obj, Float.parseFloat(valeur));
            } else if( clas.equals(new Long("0").getClass()) || clas.equals(long.class) ){
                obj.getClass().getDeclaredMethod("set"+enMajuscule(attribut),clas).invoke(obj, Long.parseLong(valeur));
            } else if( clas.equals(new String().getClass()) ){
                obj.getClass().getDeclaredMethod("set"+enMajuscule(attribut),clas).invoke(obj, valeur);
            } else if( clas.equals(new Date(0).getClass()) ){
                obj.getClass().getDeclaredMethod("set"+enMajuscule(attribut),clas).invoke(obj, Date.valueOf(valeur));
            }
        } catch (Exception e) {
           throw e;
        }
    }
    /*-----------------------------------------*/

/*----------------------------------------------------------------------------*/
}
