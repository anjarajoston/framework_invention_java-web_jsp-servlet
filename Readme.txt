readme
if you want to see just the source code: 'framework invention java-web jsp-servlet\src\java'
if you want to use: go to 'framework_invention_java-web_jsp-servlet\jar' and copy the 'framework.jar' in the library of your project

this framework processes all urls that end with '.do' ex:save.do and we must annotate with @MyAnnotation(url = "save") at the top of the method called by the url

all methods must have as return value the class ModelView(String url,Url_ClassMethod_HashMap data):
 url is the page where the response from the url will be dispatched and data is a class (Url_ClassMethod_HashMap) which wants as an attribute NomRecup: the names that we will put in the 1st argument of request.setAttribut() and valueRecup: object type the return value of the url and the value of request.setAttribut()
form = request.setAttribute(nameRecover,valueRecover)